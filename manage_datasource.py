from sqapi.api import SQAPIargparser
from sqapi.geodata_cli import GeodataCLI
from repos import *   # register all datasource_repositories

# Example how to add an argument
# SQAPIBase.add_argument("--api_path", type=str, help="The API path (test)", required=True)
# sqgeo = GeodataAPI(sq=SQAPIBase())
from sqapi.ui import UIComponents

args = SQAPIargparser.parse_args()
sqgeo = GeodataCLI(**vars(args))


actions = [
    dict(name="⤓ Import from remote repository", callback=sqgeo.sync_select_datasource_ui),
    dict(name="✎ Edit existing datasets", callback=sqgeo.list_platforms_ui)
]
UIComponents.select_action(actions, title="What would you like to do?", _push_state=True)