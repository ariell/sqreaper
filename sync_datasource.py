from sqapi.api import SQAPIargparser
from sqapi.geodata import GeodataAPI
from repos import *   # register all datasource_repositories


# Add CLI argument to request datasource_id, can be multiple
# --datasource_id 1 --datasource_id 2 --datasource_id 3
SQAPIargparser.add_argument("--datasource_id", type=int, help="INT, ID of datasource(s) to sync. Can be multiple.", action='append', default=None)

apiargs = SQAPIargparser.parse_args()
sqgeo = GeodataAPI(**vars(apiargs))


# If no datasource_id arguments, print help message to show what ID's are available
assert apiargs.datasource_id is not None, \
    "ERROR: Input argument `--datasource_id` is required. Can be applied more than once.\nValid IDs are: \n  {}".format(
        "\n  ".join([f'--datasource_id {i.get("id")} :  {i.get("name")}'
                     for i in sqgeo.sqapi.request("GET", resource="datasource", querystring_params=dict(results_per_page=100)).get("objects")])
    )

# If datasource_ids are provided, do stuff
for datasource_id in apiargs.datasource_id:
    try:
        datasource_data = sqgeo.sqapi.request("GET", resource="datasource/{id}", resource_params=dict(id=datasource_id))
        sqgeo.sync_datasource(datasource_data)
    except Exception as e:
        print("ERROR: Unable to sync 'datasource_id={}' ({}: {})".format(datasource_id, e.__class__.__name__, str(e)))