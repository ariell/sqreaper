
import requests
from bs4 import BeautifulSoup
from sqapi.datasource import register_datasource_plugin, DataSource, argparser


class HTTPDir(DataSource):
    def list_object_paths(self, url):
        ignore_links = ["Parent Directory"]
        page = requests.get(url).text
        soup = BeautifulSoup(page, 'html.parser')
        joiner = "" if url.endswith("/") else "/"
        # Parse table
        objects = []
        for tr in soup.select("tr")[3:]:    # select table tows, but skip first two
            cells = tr.select("td")         # get cells in row
            if cells:
                a = cells[1].select("a")[0]  # link in 2nd cell
                objects.append(
                    dict(path=self.get_object_path(url + joiner + a.get('href')),   # create full link to decompose
                       basename=a.get("href").replace('/', ''),
                       type="dir" if cells[0].select("img")[0].get("alt") == "[DIR]" else "file",  # check alt of icon in first cell
                       mtime=cells[2].contents[0],
                       size=cells[3].contents[0])
                )
        return objects


# Register datasource plugin
register_datasource_plugin(HTTPDir)


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = HTTPDir(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)

    # # other examples:
    # # list all campaigns
    # campaigns = ds.list_campaigns()
    # print(campaigns)
    #
    # # list all deployments
    # alldeployments = ds.list_deployments()
    # print(alldeployments)
    #
    # # list deployments from specific campaign
    # deployments = ds.list_deployments(campaign=campaigns[0])
    # print(deployments)
    #
    # # list all data files
    # alldatafiles = ds.get_datafiles()
    # print(alldatafiles)
    #
    # # list datafiles from specific campaign/deployment
    # datafiles = ds.get_datafiles(campaign=campaigns[0], deployment=deployments[0])
    # print(datafiles)

