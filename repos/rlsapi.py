from datetime import datetime, timedelta
import traceback
import atexit

import os
import requests
import pandas as pd
from parse import parse
import tempfile

from sqapi.api import HTTPException, SQAPIargparser
from sqapi.datasource import DataSource, argparser, SafeWildcardDict, register_datasource_plugin


# survey_id
# site_code
# location
# survey_date
# hour
# depth
# latitude
# longitude
# has_pqs_catalogued_in_db


# CONSTANTS
PATHSEP = "$"
CSV_DATA_URL = "http://geoserver-rls.imas.utas.edu.au/geoserver/RLS/ows?service=WFS&version=1.0.0&request=" \
               "GetFeature&typeName=RLS%3ASurveyList&outputFormat=csv"
CSV_DATA_OVERRIDE_FILE = os.path.join(os.path.dirname(__file__), "SurveyListOverride.csv")
# DATABASE_URL = "/Users/ariell/Downloads/SurveyList.csv"


# Add argument to APIBASE class


# DATETIMECOL_FMT = "%Y-%m-%dT%H:%M:%S"
DATETIMECOL_FMT = "%Y-%m-%d"
CAMPAIGNCOL_NAME = "__campaign__"
DATAPATH_PATTERN = PATHSEP+"{campaign}"+PATHSEP
DATAFILE_URL = "http://rls.tpac.org.au/pq/{fileparams[basename]}/"
# DATAFILE_URL = "http://rls.tpac.org.au/pq/923402334/"
TEMPDIR = None

# FILTERS TO APPLY ON DATABASE AFTER DOWNLOAD
FILTERS = {
    "has_pqs_catalogued_in_db": True,
    # "StateArea": "Victoria"    # TODO: remove this temporary filter - just for initial imports
}

# SQAPIargparser.add_argument("--survey_list", type=str, default=CSV_DATA_OVERRIDE_FILE,
#             help="(optional) RLSAPI datasource only. Override the list of surveys to be imported (default: {})".format(CSV_DATA_OVERRIDE_FILE))

class RLSAPI(DataSource):
    def __init__(self, *args, **kw):  # survey_list=None, *args, **kw):
        super().__init__(*args, **kw)
        self.csvdatadf = None
        self.pathsep = PATHSEP
        # self.rls_survey_list = survey_list

        # Set up a tempdir and delete it when done
        self.tempdir = tempfile.mkdtemp()
        # Register method to run on exit to delete the tempdir
        atexit.register(clear_tempfiles, self.tempdir)  #, survey_list)
        print("Created temp directory: {}".format(self.tempdir))

    def list_object_paths(self, url):
        # url = platform_data['datasource_pattern'].format(prefix=prefix)
        # print(f"list_path_objects: {path+'/'}")
        if not url.endswith(self.pathsep):
            url += self.pathsep

        try:
            p = parse(DATAPATH_PATTERN, url)
            campaign = p.named.get("campaign") if p else None
            df = self.get_csv_data(CSV_DATA_URL)

            if campaign is None:
                # print(f"Getting campaign: {url}")
                return [dict(
                    path="{}{}{}".format(url,i,self.pathsep),
                    basename=str(i),
                    type="dir",
                    mtime=None,
                    size=None
                ) for i in list(df[CAMPAIGNCOL_NAME].unique())]
            else:
                # print(f"\nGetting deployments: \nURL: {url} | CAMPAIGN: {campaign}\n")
                filt_df = df[df[CAMPAIGNCOL_NAME] == campaign]
                return [dict(
                    path="{}{}{}".format(url,r["survey_id"],self.pathsep),
                    basename=str(r["survey_id"]),
                    type="dir",
                    mtime=None,
                    size=None
                ) for i, r in filt_df.iterrows()]
        except Exception as e:
            traceback.print_exc()
            return []

    def get_csv_data(self, url):
        """
        Download csv file database for local processing
        :param url:
        :return:
        """
        if self.csvdatadf is None:
            if os.path.isfile(CSV_DATA_OVERRIDE_FILE):
                url = CSV_DATA_OVERRIDE_FILE
                print("Loading override data file: {}".format(CSV_DATA_OVERRIDE_FILE))
            else:
                print("Downloading data file: {}...".format(url))
            # print("Downloading data file: {}...".format(url))
            df = pd.read_csv(url)
            print("Got {} rows".format(df.shape[0]))
            for k,v in FILTERS.items():
                df = df[df[k] == v]
                print("Filtered: {}={} ({} rows)".format(k, v, df.shape[0]))
            df[CAMPAIGNCOL_NAME] = "RLS_" + df["location"] + "_" + \
                               df["survey_date"].map(lambda x: str(datetime.strptime(x, DATETIMECOL_FMT).year))
            self.csvdatadf = df
        return self.csvdatadf

    def preprocess_deployment_assets(self, campaign=SafeWildcardDict(), deployment=SafeWildcardDict(), *args, **kwargs):
        print(" * Preprocessing deployment assets...")
        df = self.get_csv_data(CSV_DATA_URL)
        dpl_info = df.loc[self.csvdatadf['survey_id'].astype(str) == deployment.get("basename")].to_dict(orient="records")[0]

        dpl_datetime = datetime.strptime(dpl_info.get('survey_date'), DATETIMECOL_FMT)

        # update deployment name to include site name
        deployment["name"] = "{}_{}_{}".format(
            dpl_info.get('survey_id'),
            dpl_info.get('site_code'),
            dpl_datetime.strftime('%Y-%m-%d')
        )
        deployment["fileparams"] = dict(
            datetime=dpl_datetime,
            depth=dpl_info.get("depth", None),
            alt=dpl_info.get("altitude", None),
            basename=deployment.get("basename")
        )
        return campaign, deployment

    # def postprocess_deployment_assets(self, dpl=None, *args, **kwargs):
    #     # POSTPROCESS: download and generate all files (even if not imported). This has been superceeded by `preprocess_file`
    #     # Generate temporary metadata file through RLS API query
    #     #print("Running 'postprocess_deployment_assets'")
    #     #print(dpl.datafiles)
    #     datafile_path = dpl.datafiles[0].get("path")
    #     datafile_url = DATAFILE_URL.format(deployment=dpl.deployment)
    #     r = requests.get(datafile_url, headers={"Accept": "application/json"})
    #     if r.ok:
    #         df = pd.DataFrame.from_dict(r.json().get("results"))
    #         df.drop(["height", "width", "scaled", "urls"], inplace=True, axis=1)
    #         info = dpl.deployment.get("info",{})
    #         for i, r in df.iterrows():
    #             df.at[i, "depth"] = info.get("Depth", None)
    #             df.at[i, "alt"] = info.get("Altitude", None)
    #             # add fake offset so that images have sequenced timestamps
    #             df.at[i, "timestamp"] = (dpl.deployment.get("datetime") + timedelta(seconds=float(i)*20)).strftime(DATETIMECOL_FMT)
    #         df.to_csv(datafile_path, index=False)
    #     return dpl

    def preprocess_file(self, file_path=None, file_url=None, fileparams=None):
        print(" * Preprocessing file: {}".format(file_path))
        assert isinstance(fileparams, dict), \
            "`fileparams` argument is not set. Field can be set in `deployment['fileparams']` object in `preprocess_deployment_assets`"
        datafile_url = DATAFILE_URL.format(fileparams=fileparams)
        r = requests.get(datafile_url, headers={"Accept": "application/json"})
        if r.ok:
            r_json = r.json()
            if "error" in r_json:
                raise HTTPException(r_json.get("error"), url=r.url)
            df = pd.DataFrame.from_dict(r_json.get("results"))
            df.drop(["height", "width", "scaled", "urls"], inplace=True, axis=1)
            for i, r in df.iterrows():
                df.at[i, "depth"] = fileparams.get("depth", None)
                df.at[i, "alt"] = fileparams.get("alt", None)
                # add fake offset so that images have sequenced timestamps
                df.at[i, "timestamp"] = (fileparams.get("datetime") + timedelta(seconds=float(i) * 20)).strftime(DATETIMECOL_FMT)
            df.to_csv(file_path, index=False)
        return file_path


# Register method to cleanup upon exit
def clear_tempfiles(tempdir):  #, rls_survey_list):
    """
    Remove temporary files
    :return:
    """
    from shutil import rmtree
    print(" * Removing temporary files: {}".format(tempdir))
    rmtree(tempdir, ignore_errors=True)

    if os.path.isfile(CSV_DATA_OVERRIDE_FILE):
        new_file_name = "IMPORTED-{}-{}".format(datetime.now().strftime("%Y-%m-%d"),os.path.basename(CSV_DATA_OVERRIDE_FILE))
        print("Renaming override survey list file from: {} to {}".format(os.path.basename(CSV_DATA_OVERRIDE_FILE), new_file_name))
        os.rename(CSV_DATA_OVERRIDE_FILE, os.path.join(os.path.dirname(CSV_DATA_OVERRIDE_FILE), new_file_name))


# Register datasource plugin
register_datasource_plugin(RLSAPI)


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = RLSAPI(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)

    # # other examples:
    # # list all campaigns
    # campaigns = ds.list_campaigns()
    # print(campaigns)
    #
    # # list all deployments
    # alldeployments = ds.list_deployments()
    # print(alldeployments)
    #
    # # list deployments from specific campaign
    # deployments = ds.list_deployments(campaign=campaigns[0])
    # print(deployments)
    #
    # # list all data files
    # alldatafiles = ds.get_datafiles()
    # print(alldatafiles)
    #
    # # list datafiles from specific campaign/deployment
    # datafiles = ds.get_datafiles(campaign=campaigns[0], deployment=deployments[0])
    # print(datafiles)
