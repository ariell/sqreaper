import requests
import xmltodict
from sqapi.datasource import register_datasource_plugin, DataSource, argparser
import boto3


def get_objects(data, basename):
    dir_list = data.get('CommonPrefixes', [])
    file_list = data.get('Contents', [])
    dirs = [dict(path=i.get('Prefix'),
                 basename=basename(i.get("Prefix")),
                 type="dir",
                 mtime=None,
                 size=None)
            for i in ([dir_list] if isinstance(dir_list, dict) else dir_list)]
    files = [dict(path=i.get("Key"),
                  basename=basename(i.get("Key")),
                  type="file",
                  mtime=str(i.get("LastModified")),
                  size=i.get("Size"))
             for i in ([file_list] if isinstance(file_list, dict) else file_list)]
    return dirs + files


class AWS(DataSource):
    def list_object_paths(self, url):
        page = requests.get(url).text
        try:
            data = xmltodict.parse(page).get('ListBucketResult', {})
            return get_objects(data, basename=self.get_object_basename)
        except Exception as e:
            print("\nERROR: XML could not be parsed, {}\n".format(e))
            return []


class AWS_BOTO3(DataSource):
    # s3://umi-vars-s3/IN2018_V06/MRITC/IN2018_V06_012/data/MRITC_MRG_IN2018_V06_012.CSV
    # s3://umi-vars-s3/IN2018_V06/MRITC/IN2018_V06_012/stills/MRITC_SCP_IN2018_V06_012_20181124T231157Z_0001.JPG
    # s3://umi-vars-s3/IN2018_V06/MRITC/IN2018_V06_012/thumbnails/MRITC_SCP_IN2018_V06_012_20181124T231157Z_0001_THUMB.JPG
    # urlbase_browse = <BUCKET_NAME>:{path}
    def list_object_paths(self, url):
        try:
            client = boto3.client(
                's3',
                aws_access_key_id=self.credentials.get('aws_access_key_id'),
                aws_secret_access_key=self.credentials.get('aws_secret_access_key'))
            data = client.list_objects(Bucket=self.credentials.get('bucket'), Delimiter='/', Prefix=url)
            return get_objects(data, basename=self.get_object_basename)
        except Exception as e:
            print("\nERROR: BOTO could not retrieve bucket data, {}\n".format(e))
            return []

# Register datasource plugin
register_datasource_plugin(AWS, name="AWSS3")
register_datasource_plugin(AWS_BOTO3, name="AWSS3_BOTO")


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = AWS(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)

    # # other examples:
    # # list all campaigns
    # campaigns = ds.list_campaigns()
    # print(campaigns)
    #
    # # list all deployments
    # alldeployments = ds.list_deployments()
    # print(alldeployments)
    #
    # # list deployments from specific campaign
    # deployments = ds.list_deployments(campaign=campaigns[0])
    # print(deployments)
    #
    # # list all data files
    # alldatafiles = ds.get_datafiles()
    # print(alldatafiles)
    #
    # # list datafiles from specific campaign/deployment
    # datafiles = ds.get_datafiles(campaign=campaigns[0], deployment=deployments[0])
    # print(datafiles)

