import glob
import json
import os
# from PIL import Image

import numpy as np
from dateutil import parser
from parse import parse

from sqapi.datasource import DataSource, argparser, SafeWildcardDict, register_datasource_plugin
from .localfs import LocalFS
import pandas as pd

OUTPUT_DIRECTORY = "/mnt/proc/sqdata"
# OUTPUT_DIRECTORY = "/Users/ariell/Documents/data/csiro-investigator-test/outputs"
LOGFILE_POSE_COLS = ["lat", "lon", "dep", "alt"]
GBBCSV_FNAME = "navlog.gbb.csv"
IMGLOG_FNAME = "image_navlog.csv"
IMG_SIZE = [3840, 3840]
THM_SIZE = [350, 350]
IMGFNAME_FMT = 'DTC_DS{}_{}_{}_{}_{datetime}_{}.JPG'
# IMGFNAME_FILT = "DTC_DS*.JPG"   # All frames
# IMGFNAME_FILT = "DTC_DSS_*.JPG"   # Starboard frames
IMGFNAME_FILT = "DTC_DSP_*.JPG"   # Port frames


class RVInvestigatorDTLocalNavLog(LocalFS):
    def preprocess_deployment_assets(self, campaign=None, deployment=None, *args, **kwargs):
        if deployment is None:
            deployment = SafeWildcardDict()
        if campaign is None:
            campaign = SafeWildcardDict()

        deployment["fileparams"] = dict(campaign_name=campaign.get('name'),
                                        deployment_name=deployment.get('name'),
                                        deployment_path=deployment.get('path'))
        return campaign, deployment

    def preprocess_file(self, file_path=None, file_url=None, fileparams=None):
        campaign_name = fileparams.get('campaign_name')
        deployment_name = fileparams.get('deployment_name')
        deployment_path = fileparams.get('deployment_path')

        # Get file matching 'DTC_*.log' pattern
        # datafile_pattern = os.path.join(deployment.get('path'), 'log', 'DTC_*.log')
        # datafile_path = glob.glob(datafile_pattern)
        # assert len(datafile_path) == 1, \
        #     f"{len(datafile_path)} data files matching '{datafile_pattern}' were found. Expected only 1."
        img_output_dir = os.path.join(OUTPUT_DIRECTORY, campaign_name, deployment_name, "media")
        imglist = _get_image_list(deployment_path, img_output_dir)

        log_output_dir = os.path.join(OUTPUT_DIRECTORY, campaign_name, deployment_name, "logs")
        if file_path.endswith(".log") or file_path.ends_with(".csv"):
            file_path = _process_logfile_csv(file_path, log_output_dir, imglist)
        elif file_path.endswith(".json"):
            file_path = _process_logfile_json(file_path, log_output_dir, imglist)
        return file_path


def _get_image_list(deployment_path, img_output_dir):
    # Using list of files text file (for testing)
    # imglist = pd.read_csv(os.path.join(deployment_path, "stills_list.txt"), sep=",", header=None)[0].tolist()

    # # Using files in a directory
    imglist = glob.glob(os.path.join(deployment_path, "stills", IMGFNAME_FILT))

    # # Convert all images to smaller, more manageble file sizes
    # imgdir = os.path.join(img_output_dir, "full")
    # thmdir = os.path.join(img_output_dir, "thms")
    # os.makedirs(imgdir, exist_ok=True)
    # os.makedirs(thmdir, exist_ok=True)
    # for i in imglist:
    #     image = Image.open(i)
    #     image.thumbnail(IMG_SIZE, Image.ANTIALIAS)
    #     image.save(os.path.join(imgdir, os.path.basename(i)), "JPEG")
    #     image.thumbnail(THM_SIZE, Image.ANTIALIAS)
    #     image.save(os.path.join(thmdir, os.path.basename(i)), "JPEG")

    # return [dict(
    #     key=os.path.splitext(os.path.basename(i))[0],
    #     datetime=parser.parse(parse(IMGFNAME_FMT, os.path.basename(i)).named.get('datetime'))) for i in imglist]

    return imglist


def _get_image_log(logdf, imglist):
    # Build image list nav file
    dictlist = []
    log_cols = logdf.columns
    for i in imglist:
        key = os.path.splitext(os.path.basename(i))[0]
        datetime = parser.parse(parse(IMGFNAME_FMT, os.path.basename(i)).named.get('datetime'))
        dt = datetime.replace(tzinfo=None)
        obj = dict(key=key, timestamp_start=str(datetime))
        idx = logdf.index.get_loc(dt, method='nearest')
        row = logdf.iloc[idx]
        matched_timestamp = logdf.index[idx].replace(tzinfo=None)
        match_offset = (dt - matched_timestamp).total_seconds()
        obj['pose.data.time_match_error'] = match_offset
        for c in log_cols:
            value = row[c]
            if c in LOGFILE_POSE_COLS:
                obj['pose.' + c] = value  # map column to row
            elif isinstance(value, float):
                obj['pose.data.' + c] = value
        dictlist.append(obj)
    return pd.DataFrame(dictlist)


def _process_logfile_csv(logfile_path, output_dir, imglist):
    """

    @param logfile_path:
    @param output_dir:
    @param imglist:
    @return:
    """
    # Read the log file, skipping bad lines and liberal on the datatypes
    print(f"Opening csv file: {logfile_path}...")
    df = pd.read_csv(logfile_path, skiprows=1, on_bad_lines='skip', index_col=False, dtype='unicode')

    # Filter, clean and convert
    print("Filtering, cleaning and converting...")
    df = df.loc[df['USBL GPGGA'] != '<no response>']    # remove rows without USBL data
    df.replace('<no response>', np.nan, inplace=True)     # replace no response with None
    df['datetime'] = pd.to_datetime(df['Unnamed: 57'] + ' ' + df['Unnamed: 26'], format='%d/%m/%y %H%M%S.%f')  # get datetime from winch date and GPGGA UTC usbl time
    # df['datetime'] = df['datetime'].apply(pd.to_datetime, format='%d/%m/%y %H%M%S.%f')
    mbar2dep = 1/0.980655   # 10000/(1023.6*9.81)  # 10000mbar : 1Pa, density of saltwater, gravity
    df['dep'] = df['CTD Pressure'].apply(lambda x: float(x)*mbar2dep)        # Convert pressure in mbar to depth

    # Convert latlons for DT and ship
    print(df)
    print("Converting latlons for DT and ship...")
    df['lat'] = df.apply(lambda x: (float(x['Unnamed: 27'][0:2]) + float(x['Unnamed: 27'][2:])/60) * (-1 if x['Unnamed: 28']=='S' else 1), axis=1)
    df['lon'] = df.apply(lambda x: (float(x['Unnamed: 29'][0:3]) + float(x['Unnamed: 29'][3:]) / 60) * (-1 if x['Unnamed: 30'] == 'W' else 1), axis=1)
    df['ship_lat'] = df.apply(lambda x: (float(x['Unnamed: 12'][0:2]) + float(x['Unnamed: 12'][2:]) / 60) * (-1 if x['Unnamed: 13'] == 'S' else 1), axis=1)
    df['ship_lon'] = df.apply(lambda x: (float(x['Unnamed: 14'][0:3]) + float(x['Unnamed: 14'][3:]) / 60) * (-1 if x['Unnamed: 15'] == 'W' else 1), axis=1)

    # Define column mappers
    # Build new df and prune to have unique timestamps
    print("Define column mappers to build new df and prune to have unique timestamps...")
    df = pd.DataFrame({
        'datetime': df['datetime'],
        'lat': df['lat'],
        'lon': df['lon'],
        'alt': df['Altimeter'],
        'dep': df['dep'],
        'temp': df['CTD Temperature'].apply(pd.to_numeric, errors='ignore'),
        'conductivity': df['CTD Conductivity'].apply(pd.to_numeric, errors='ignore'),
        'oxygen': df['CTD Oxygen'].apply(pd.to_numeric, errors='ignore'),
        'salinity': df['CTD Salinity'].apply(pd.to_numeric, errors='ignore'),
        'sound_vel': df['CTD Sound Velocity'].apply(pd.to_numeric, errors='ignore'),
        'roll': df['Roll'].apply(pd.to_numeric, errors='ignore'),
        'pitch': df['Pitch'].apply(pd.to_numeric, errors='ignore'),
        'yaw': df['Yaw'].apply(pd.to_numeric, errors='ignore'),
        'gpgga_quality': df['Unnamed: 31'].apply(pd.to_numeric, errors='ignore'),
        'ship_lat': df['ship_lat'].apply(pd.to_numeric, errors='ignore'),
        'ship_lon': df['ship_lon'].apply(pd.to_numeric, errors='ignore')
    })

    # simplify to unique timestamps taking last (most up to date entry)
    df = df.groupby("datetime").last()

    # Save output as a gbb.csv file
    print("Saving output as a gbb.csv file...")
    os.makedirs(output_dir, exist_ok=True)
    df.to_csv(os.path.join(output_dir, GBBCSV_FNAME))

    # Build image list nav file
    print("Building image list nav file...")
    imgdf = _get_image_log(df, imglist)
    imgdf.to_csv(os.path.join(output_dir, IMGLOG_FNAME), index=False)

    return os.path.join(output_dir, IMGLOG_FNAME)


def _process_logfile_json(file_path, log_output_dir, imglist):
    pass


register_datasource_plugin(RVInvestigatorDTLocalNavLog)


