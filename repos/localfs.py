import glob
import os
from datetime import datetime

from sqapi.datasource import register_datasource_plugin, DataSource, argparser


class LocalFS(DataSource):
    def list_object_paths(self, url):
        pass

    def get_matched_objects(self, path_pattern):
        return [dict(path=self.get_object_path(p),
                     basename=self.get_object_basename(p),
                     type="dir" if os.path.isdir(p) else "file",
                     mtime=datetime.fromtimestamp(os.path.getmtime(p)),
                     size=os.stat(p).st_size)
                for p in glob.glob(path_pattern)]


# Register datasource plugin
register_datasource_plugin(LocalFS)


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = LocalFS(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)

    # # other examples:
    # # list all campaigns
    # campaigns = ds.list_campaigns()
    # print(campaigns)
    #
    # # list all deployments
    # alldeployments = ds.list_deployments()
    # print(alldeployments)
    #
    # # list deployments from specific campaign
    # deployments = ds.list_deployments(campaign=campaigns[0])
    # print(deployments)
    #
    # # list all data files
    # alldatafiles = ds.get_datafiles()
    # print(alldatafiles)
    #
    # # list datafiles from specific campaign/deployment
    # datafiles = ds.get_datafiles(campaign=campaigns[0], deployment=deployments[0])
    # print(datafiles)

